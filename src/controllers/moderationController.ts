import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import redis from '../services/redisClient';

import { invalidateObjects } from '../retriever/user';
import { invalidateObject as invalidateThreadObject } from '../retriever/thread';
import moderatorOnly from '../helpers/moderatorOnly';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from "../../config/server";
import * as eventLogController from './eventLogController';
import { invalidateObject } from '../retriever/userProfile';

export const getIpsByUsername = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const ipAddressResults = await knex
      .select(
        'Posts.id',
        'Posts.ip_address',
        'Users.username',
        'Users.id as userId',
        knex.raw('COUNT(*) as count')
      )
      .from('Users')
      .leftJoin('Posts', 'Users.id', 'Posts.user_id')
      .whereRaw('Users.username LIKE ?', [`%${req.body.username}%`])
      .groupBy('Posts.ip_address')
      .orderBy('Posts.created_at', 'desc');

    const response = Array.isArray(ipAddressResults) ? ipAddressResults : [ipAddressResults];

    if (!response[0]) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    // const banResults = await knex
    //   .select(
    //     'Bans.expires_at as expiresAt',
    //     'Bans.created_at as createdAt',
    //     'Bans.ban_reason as banReason',
    //     'Bans.banned_by as bannedBy'
    //   )
    //   .from('Bans')
    //   .whereRaw('Bans.user_id = ?', [userId]);

    // const bans = Array.isArray(banResults) ? banResults : [banResults];

    // format ip_address field for the frontend
    const userInfo = response.reduce((acc, val) => {
      if (!acc[val.userId]) {
        acc[val.userId] = {
          id: val.userId,
          username: val.username,
          ipAddresses: []
        };
      }

      if (val.ip_address) {
        acc[val.userId].ipAddresses = [...acc[val.userId].ipAddresses, val.ip_address];
      }

      return acc;
    }, {});

    res.status(httpStatus.OK);
    // transform object into array
    res.json([...Object.values(userInfo)]);
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const getUsernamesByIp = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const result = await knex
      .select('Posts.ip_address', 'Users.username', 'Users.id as id')
      .from('Users')
      .leftJoin('Posts', 'Users.id', 'Posts.user_id')
      .whereRaw('Posts.ip_address LIKE ?', [`%${req.body.ipAddress}%`])
      .groupBy('Users.username')
      .orderBy('Posts.created_at', 'desc');

    const response = Array.isArray(result) ? result : [result];

    res.status(httpStatus.CREATED);
    res.json(response);
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const getOpenReports = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const result = await knex('Reports')
      .select(
        'Reports.id as reportId',
        'Reports.post_id',
        'Reports.reported_by as reportedBy',
        'Reports.report_reason as reportReason',
        'reportuser.username as reportUsername',
        'Posts.id as postId',
        'Posts.content as postContent',
        'Posts.user_id as postUserId',
        'Posts.created_at as postCreatedAt',
        'Posts.updated_at as postUpdatedAt',
        'Posts.thread_id as postThreadId',
        'postuser.username as postUsername',
        'postuser.avatar_url as avatarUrl',
        'postuser.created_at as userJoinDate',
        'ban.ban_reason as banReason',
        'ban.banned_by as banBannedBy',
        'ban.created_at as banCreatedAt',
        'ban.expires_at as banExpiresAt',
        'Subforums.name as subforumName',
        knex.raw(
          '(select count (*) from Posts popos where popos.thread_id = Posts.thread_id AND popos.created_at < Posts.created_at) as olderPosts'
        ),
        knex.raw(
          '(select count (*) from Bans where Bans.user_id = Posts.user_id AND expires_at > NOW()) as postUserBans'
        )
      )
      .where('solved_by', null)
      .leftJoin('Posts', 'Posts.id', 'Reports.post_id')
      .leftJoin('Threads', 'Posts.thread_id', 'Threads.id')
      .leftJoin('Subforums', 'Threads.subforum_id', 'Subforums.id')
      .leftJoin('Users as postuser', 'Posts.user_id', 'postuser.id')
      .leftJoin('Users as reportuser', 'Reports.reported_by', 'reportuser.id')
      .leftJoin('Bans as ban', 'ban.post_id', 'Reports.post_id')
      .catch(err => console.log(err));

    const response = Array.isArray(result) ? result : [result];

    const responseWithPage = response.map(report => {
      report.postPage = Math.floor(report.olderPosts / 20) + 1;
      return report;
    });

    res.status(httpStatus.CREATED);
    res.json(responseWithPage);
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const closeReport = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const result = await knex('Reports')
      .where({ id: req.body.reportId })
      .update({ solved_by: req.user.id, updated_at: knex.fn.now() });

    res.status(httpStatus.CREATED);
    res.json({ message: 'Report marked as solved.' });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const changeThreadStatus = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const result = await knex('Threads')
      .where({ id: req.body.threadId })
      .update(req.body.changedData);

    const event = await eventLogController.threadStatusEvent({
      userId: req.user.id,
      username: req.user.username,
      threadId: req.body.threadId,
      type: req.body.changedData
    });

    await invalidateThreadObject(req.body.threadId);

    res.status(httpStatus.CREATED);
    res.json({ message: 'Thread status updated.' });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const countOpenReportsQuery = async () => {
  const result = await knex('Reports')
    .count('id')
    .where('solved_by', null)
    .then(result => result[0])
    .catch(err => console.log(err));

  return result['count(`id`)'];
}

export const countOpenReports = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const result: number = await countOpenReportsQuery();

    res.status(httpStatus.OK);
    res.json({ message: result });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const removeUserImage = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    if (!req.body.userId) {
      throw new Error('No user id provided.');
    }
    if (!req.body.avatar && !req.body.background) {
      throw new Error('No data requested for removal.');
    }

    const fields = {
      avatar_url: req.body.avatar ? 'none.webp' : undefined,
      background_url: req.body.background ? '' : undefined
    };

    const result = await knex('Users')
      .where({ id: req.body.userId })
      .update({
        ...fields
      });

    let fileName = req.body.userId + ((req.body.background) ? '-bg.webp' : '.webp');
    let filePath = (NODE_ENV === "production") ? 'image/' + fileName : 'avatars/' + fileName;
    await fileStore.deleteFile(filePath);

    invalidateObjects([req.body.userId]);

    const event = await eventLogController.removeUserAvatarBg({
      userId: req.body.userId,
      avatar: req.body.avatar,
      background: req.body.background,
      removedBy: req.user.id
    });

    res.status(httpStatus.OK);
    res.json({ message: 'User images updated.' });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const removeUserProfile = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    if (!req.body.userId) {
      throw new Error('No user id provided.');
    }

    const result = await knex('UserProfiles')
      .where({ user_id: req.body.userId })
      .del();

    // uncache the user profile
    invalidateObject(req.body.userId);

    // add to the event log
    await eventLogController.removeUserProfile({
      userId: req.body.userId,
      removedBy: req.user.id
    });

    res.status(httpStatus.OK);
    res.json({ message: 'User profile updated.' });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const getLatestUsers = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const userListResults = await knex
      .select('*')
      .from('Users')
      .orderBy('Users.created_at', 'desc')
      .limit(20);

    const response = Array.isArray(userListResults) ? userListResults : [userListResults];

    if (!response[0]) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    res.status(httpStatus.OK);
    res.json(response);
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const getDashboardData = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const cacheKey = 'dashboardData';
    let dashboardData: string | object = await redis.getAsync(cacheKey);

    if (typeof dashboardData === 'string') {
      dashboardData = JSON.parse(dashboardData);
    } else {
      const newUserCount = await knex
        .count('id as count')
        .from('Users')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then(result => result[0].count)
        .catch(err => console.log(err));

      const newThreadCount = await knex
        .count('id as count')
        .from('Threads')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then(result => result[0].count)
        .catch(err => console.log(err));

      const newPostCount = await knex
        .count('id as count')
        .from('Posts')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then(result => result[0].count)
        .catch(err => console.log(err));

      const reportsCount = await knex('Reports')
        .count('id as count')
        .where('solved_by', null)
        .then(result => result[0].count)
        .catch(err => console.log(err));

      const newBanCount = await knex
        .count('id as count')
        .from('Bans')
        .whereRaw('created_at >= now() - INTERVAL 1 DAY')
        .then(result => result[0].count)
        .catch(err => console.log(err));

      dashboardData = {
        threads: newThreadCount,
        users: newUserCount,
        posts: newPostCount,
        reports: reportsCount,
        bans: newBanCount
      };

      redis.setex(cacheKey, 10, JSON.stringify(dashboardData));
    }

    res.status(httpStatus.OK);
    res.json(dashboardData);
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const getFullUserInfo = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const user = await knex
      .select('*')
      .from('Users')
      .where('id', req.body.userId);

    if (!user[0]) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    const bans = await knex
      .select('Bans.*', 'Users.username as bannedBy')
      .from('Bans')
      .where('user_id', req.body.userId)
      .leftJoin('Users', 'Users.id', 'Bans.banned_by');

    const ipAddresses = await knex
      .select('Posts.id', 'Posts.ip_address', knex.raw('COUNT(*) as count'))
      .from('Users')
      .leftJoin('Posts', 'Users.id', 'Posts.user_id')
      .whereRaw('Users.id = ?', [req.body.userId])
      .groupBy('Posts.ip_address')
      .orderBy('Posts.created_at', 'desc');

    const result = {
      ...user[0],
      bans,
      ipAddresses
    };

    res.status(httpStatus.OK);
    res.json(result);
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const makeBanInvalid = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    const originalBan = await knex('Bans')
      .select('*')
      .where({ id: req.body.banId });

    const createdAt = originalBan[0].created_at;
    const expiresAt = originalBan[0].expires_at;

    const newCreatedAt = new Date(new Date(createdAt).setFullYear(2001));
    const newExpiresAt = new Date(new Date(expiresAt).setFullYear(2001));

    console.log({ created_at: newCreatedAt, expires_at: newExpiresAt, id: originalBan[0].id });

    const updatedBan = await knex('Bans')
      .update({ created_at: newCreatedAt, expires_at: newExpiresAt })
      .where({ id: originalBan[0].id });

    await invalidateObject(req.body.userId);

    const event = await eventLogController.unbanUser({
      userId: req.body.userId,
      removedBy: req.user.id
    });

    res.status(httpStatus.OK);
    res.json({ message: 'Ban year set to 2001.' });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: exception });
    }
  }
};

export const addGoldProduct = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);

    if (!req.user || !req.user.usergroup || req.user.usergroup !== 4 || req.user.id !== 1) {
      throw new Error('error')
    }

    const date = new Date();
    date.setMonth(date.getMonth() !== 11 ? date.getMonth() + 1 : 0);
    const isoDate = date.toISOString();

    const insertedSubscription = await knex('ProductSubscriptions')
      .insert({ user_id: req.body.userId, product_id: 1, expires_at: date })

    res.status(httpStatus.OK);
    res.json({ message: 'Ban year set to 2001.' });
  } catch (exception) {
    console.error(exception);
    if (!res.headersSent) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'error' });
    }
  }
};
