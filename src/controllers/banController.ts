import { Request, Response } from "express";

import httpStatus from 'http-status';
import knex from '../services/knex';

import moderatorOnly from '../helpers/moderatorOnly';
import * as eventLogController from './eventLogController';
import { invalidateObject } from "../retriever/user";

export const store = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);
    const { banLength } = req.body;

    let expiresAt = new Date();
    const parsedBanLength = parseInt(banLength);

    if (parsedBanLength <= 0) {
      // If the ban length is 0 or less, perma ban
      expiresAt.setUTCFullYear(2100);
    } else {
      expiresAt.setUTCHours(expiresAt.getUTCHours() + parsedBanLength)
    }

    await knex('Bans')
      .insert({
        user_id: req.body.userId,
        expires_at: expiresAt,
        ban_reason: req.body.banReason,
        post_id: req.body.postId,
        banned_by: req.user.id
      });

    eventLogController.banEvent({
      userId: req.body.userId,
      expiresAt: `${expiresAt.toUTCString()}`,
      banReason: req.body.banReason,
      postId: req.body.postId,
      bannedBy: req.user.id
    });

    await invalidateObject(req.body.userId);

    res.status(httpStatus.CREATED);
    res.json({ message: 'User banned. Unban date: ' + expiresAt.toUTCString(), banExpiresAt: expiresAt.getTime() });
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};
