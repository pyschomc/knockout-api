import { Request, Response } from 'express';
import httpStatus from 'http-status';
import composer from '../helpers/queryComposer';
import ResponseStrategy from '../helpers/responseStrategy';
import datasource from '../models/datasource';
import knex from '../services/knex';

import { getAll } from './subforum/getAll';
import { getThreads } from './subforum/getThreads';

const {
  models: { Subforum, Thread, User, Post },
  Sequelize
} = datasource();

export const store = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Subforum);

  try {
    const subforum = await Subforum.create(req.body);

    const result = await scope.findOne({ where: { id: subforum.id } });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};
export const update = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Subforum);

  try {
    const subforum = await Subforum.findOne({ where: req.params });

    await subforum.update(req.body);

    const result = await scope.findOne({ where: req.params });

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};
export const destroy = async (req: Request, res: Response) => {
  Subforum.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};
export const count = (req: Request, res: Response) => {
  const options = composer.onlyQuery(req);

  Subforum.count(options)
    .then(result => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch(err => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};
