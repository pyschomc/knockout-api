import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { POSTS_PER_PAGE } from '../../config/server';
import Pagination from '../helpers/PaginationBuilder';
import composer from '../helpers/queryComposer';
import { default as ResponseStrategy } from "../helpers/responseStrategy";
import datasource from '../models/datasource';
import { getFormattedObjects, invalidateObject, invalidateObjects, getFormattedObject } from '../retriever/thread';
import knex from '../services/knex';
import redis from '../services/redisClient';
import { validatePostLength } from '../validations/post';
import { validateThreadTitleLength, validateThreadTagCount } from '../validations/thread';
import { validateUserFields, validateUserAge } from '../validations/user';
import { isProxy } from '../helpers/proxyDetect';
import * as eventLogController from './eventLogController';
import * as postController from './postController';
import convertSlateToBBCode from '../helpers/slateToBbCode';

const { Thread, Rating } = datasource().models;

const MIN_ACCT_AGE_SUBFORUM_IDS = [5, 6]; // subforumIds where acct has to be at least 3 days old to post

export const index = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Thread);
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = 'Thread.id';

  try {
    const result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    ResponseStrategy.send(res, pagResult, options);
  } catch (error) {
    console.error(error);
    ResponseStrategy.send(res, error, options);
  }
};

export const popular = async (req: Request, res: Response) => {
  const threadsCache = await redis.getAsync('popular-threads');

  if (threadsCache) {
    return ResponseStrategy.send(res, { list: JSON.parse(threadsCache) });
  }

  const query = await knex
    .from('Posts as po')
    .select('th.id', knex.raw('count(DISTINCT(po.user_id)) as replyCount'))
    .whereRaw(
      'po.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY) AND locked = 0 AND th.deleted_at IS NULL'
    )
    .limit(20)
    .leftJoin('Threads as th', 'po.thread_id', 'th.id')
    .join('Users as us', 'us.id', 'th.user_id')
    .groupBy('th.id')
    .orderByRaw('replyCount DESC');

  const queryResults = Array.isArray(query) ? query : [query];
  const threadIds = queryResults.map(item => {
    return item.id;
  });

  const threads = await getFormattedObjects(threadIds);

  redis.setex('popular-threads', 60, JSON.stringify(threads));

  return ResponseStrategy.send(res, { list: threads });
};

export const latest = async (req: Request, res: Response) => {
  const query = await knex
    .from('Threads as th')
    .select('th.id')
    .whereRaw('locked = 0 AND th.deleted_at IS NULL')
    .limit(20)
    .orderByRaw('th.id DESC');

  const queryResults = Array.isArray(query) ? query : [query];
  const threadIds = queryResults.map(item => {
    return item.id;
  });

  const threads = await getFormattedObjects(threadIds);
  return ResponseStrategy.send(res, { list: threads });
};

export const show = async (req: Request, res: Response) => {
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = 'Thread.id';
  options.parameters = [req.query.postPage, req.params.id];

  const scope = composer.scope(req, Thread, options);

  try {
    const result = await scope.findOne(options);

    redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

    ResponseStrategy.send(res, result, options);
  } catch (error) {
    console.error(error);
    ResponseStrategy.send(res, error, options);
  }
};

export const store = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Thread);

  if (req.user.isBanned) {
    throw new Error('User is banned');
  }

  if (
    !req.body.title ||
    !validateThreadTitleLength(req.body.title) ||
    !validatePostLength(req.body.content) ||
    !validateThreadTagCount(req.body.tagIds)
  ) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'Invalid content' });
    return;
  }

  if (!validateUserFields(req.user)) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({ error: 'Invalid user' });
    return;
  }

  // is the user under a week old?
  const isNew = !(await validateUserAge(req.user));
  if (isNew) {
    // are they using a VPN or do they look like they're in a datacenter?
    const ip = req.ipInfo;
    if (isProxy(ip)) {
      console.log('VPN detected', ip);
      throw new Error('You appear to be using a proxy/VPN.');
    }
    
    // is this user trying to post in News/Politics?
    const thread = await getFormattedObject(req.body.thread_id);
    if (MIN_ACCT_AGE_SUBFORUM_IDS.includes(req.body.subforumId)) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      throw new Error(`Your account (${req.user.id} - ${req.user.username}) is too new to post in this subforum. (ERR2NU: LURKMOAR)`);
    }
  }

  // validations for a valid post
  if (!validatePostLength(req.body.content)) {
    throw new Error('Failed validations.');
  }

  try {
    // check if user is a gold member. if he's not and sent a bg url,
    // boot him.
    const allowedUsergroups = [2, 3, 4, 5];
    if (!allowedUsergroups.includes(req.user.usergroup) && req.body.backgroundUrl) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: "Forbidden" });
      throw new Error("Not authorized.");
    }

    const thread = await Thread.create({ ...req.body, user_id: req.user.id, background_url: req.body.backgroundUrl, background_type: req.body.backgroundType });

    postController.store(
      { ...req, returnEarly: true, body: { ...req.body, thread_id: thread.id } } as Request,
      res
    );

    // Insert thread tags
    if (req.body.tagIds && req.body.tagIds.length > 0) {
      await knex('ThreadTags')
        .insert(req.body.tagIds.map(tagId => ({
          tag_id: tagId,
          thread_id: thread.id
        })
        ));
    }

    const result = await scope.findOne({ where: { id: thread.id } });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const update = async (req: Request, res: Response) => {
  const allowedUsergroups = [3, 4, 5];

  // this is a logged in only action
  if (!req.isLoggedIn || !req.user || req.user.isBanned) {
    res.status(httpStatus.FORBIDDEN);
    res.json({ error: 'Forbidden' });
    return;
  }

  const scope = composer.scope(req, Thread);

  try {
    const thread = await Thread.findOne({ where: { id: req.body.id } });
    const oldTitle = thread.title;

    // if this user is not a mod and not the author of the thread, he cant edit it
    if (!allowedUsergroups.includes(req.user.usergroup) && thread.user_id !== req.user.id) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    interface UpdatePayloadInterface { title?: string, background_url?: string, background_type?: string, subforum_id?: number };

    const updatePayload: UpdatePayloadInterface = {};

    if (req.body.title) {
      if (!validateThreadTitleLength(req.body.title)) {
        res.status(httpStatus.FORBIDDEN);
        res.json({ error: 'Invalid thread title' });
        return;
      }

      updatePayload.title = req.body.title;
    }

    if (req.body.subforum_id && allowedUsergroups.includes(req.user.usergroup)) {
      updatePayload.subforum_id = req.body.subforum_id;
    }

    if (req.body.backgroundUrl || req.body.backgroundType) {
      // check if user is a gold member. if he's not and sent a bg url/type, boot him.
      const allowedUsergroupsWithGold = [2, 3, 4, 5];

      if (!allowedUsergroupsWithGold.includes(req.user.usergroup)) {
        res.status(httpStatus.FORBIDDEN);
        res.json({ error: "Forbidden" });
        throw new Error("Not authorized.");
      }

      if (req.body.backgroundUrl) {
        updatePayload.background_url = req.body.backgroundUrl;
      }

      if (req.body.backgroundType) {
        updatePayload.background_type = req.body.backgroundType;
      }
    }

    if (thread.locked === false) {
      await thread.update(updatePayload);
    } else {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'You can\'t do that, dummy' });
      return;

    }

    await invalidateObject(req.body.id);

    const result = await scope.findOne({ where: { id: req.body.id } });

    if (updatePayload.title) {
      eventLogController.threadUpdateEvent({
        username: req.user.username,
        oldTitle,
        newTitle: result.title,
        threadId: req.body.id,
        userId: req.user.id,
        body: req.body
      });
    }

    if (updatePayload.background_url) {
      eventLogController.threadBackgroundUpdateEvent({
        username: req.user.username,
        threadTitle: thread.title,
        threadId: req.body.id,
        userId: req.user.id
      });
    }

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};

export const destroy = async (req: Request, res: Response) => {
  Thread.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};

export const count = (req: Request, res: Response) => {
  const options = composer.onlyQuery(req);

  Thread.count(options)
    .then(result => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch(err => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  // if no page parameter is provided, default to page 1
  const pageParam: number = Number(req.params.page) || 1;

  try {
    // get total posts for this thread
    const PAGE_POSTS: number = await knex
      .count()
      .from('Posts as po')
      .where(knex.raw('po.thread_id = ?', [req.params.id]))
      .map(c => c['count(*)'])
      .then(c => c[0]);

    // note that posts per page is hardcoded here, but not on the frontend.
    const PAGE_HAS_POSTS = PAGE_POSTS - parseInt(POSTS_PER_PAGE as string, 10) * (pageParam - 1) > 0;

    const currentUserGroup = req.user && req.user.usergroup ? req.user.usergroup : 0;
    const userIsMod = [3, 4, 5].includes(currentUserGroup);

    const andDeletedIsNull = req.user && userIsMod ? '' : `and th.deleted_at is null`;

    if (PAGE_HAS_POSTS) {
      // this whole thing SERIOUSLY, SERIOUSLY need to be refactored.
      // it is by far the worst single piece of code on the entire project
      const query = await knex
        .from('Threads as th')
        .select(
          'th.id as threadId',
          'th.title as threadTitle',
          'th.icon_id as threadIconId',
          'th.user_id as threadUserId',
          'th.subforum_id as threadSubforumId',
          'sf.name as threadSubforumName',
          'th.created_at as threadCreatedAt',
          'th.updated_at as threadUpdatedAt',
          'th.deleted_at as threadDeletedAt',
          'th.locked as threadLocked',
          'th.pinned as threadPinned',
          'th.background_url as threadBackgroundUrl',
          'th.background_type as threadBackgroundType',
          'po.id as postId',
          'po.content as postContent',
          'po.user_id as postUserId',
          'po.created_at as postCreatedAt',
          'po.updated_at as postUpdatedAt',
          'po.thread_id as postThreadId',
          'po.country_name as postCountryName',
          'us.username as userUsername',
          'us.usergroup as userUsergroup',
          'us.avatar_url as userAvatar_url',
          'us.background_url as userBackgroundUrl',
          'us.created_at as userCreated_at',
          knex.raw(
            "GROUP_CONCAT(CONCAT(rat.user_id, ':', rat.rating_id) SEPARATOR ', ') AS ratings"
          ),
          knex.raw(
            '(select count (*) from Bans where Bans.user_id = po.user_id AND expires_at > now()) as userBanCount'
          )
        )
        .leftJoin('Posts as po', 'po.thread_id', 'th.id')
        .leftJoin('Ratings as rat', 'po.id', 'rat.post_id')
        .leftJoin('Subforums as sf', 'th.subforum_id', 'sf.id')
        .join('Users as us', 'us.id', 'po.user_id')
        .where(knex.raw(`th.id = ? ${andDeletedIsNull}`, [req.params.id]))
        .limit(20)
        .groupBy('po.id')
        .offset(pageParam ? (pageParam - 1) * 20 : 0);

      const queryResults = Array.isArray(query) ? query : [query];

      const postIds = queryResults.map(post => post.postId);

      // Get the bans for this post so we can build the response for multi ban posts
      const bans = await knex
        .from('Bans as ban')
        .select(
          'ban.ban_reason as banReason',
          'ban.created_at as banCreatedAt',
          'ban.expires_at as banExpiresAt',
          'ban.post_id as banPostId',
          'userBannedBy.username as banBannedBy'
        )
        .whereIn('ban.post_id', postIds)
        .leftJoin('Users as userBannedBy', 'ban.banned_by', 'userBannedBy.id')
        .then(results => {
          let resultArray = Array.isArray(results) ? results : [results];

          return resultArray.reduce((bans, ban) => {
            if (bans[ban.banPostId] === undefined) {
              bans[ban.banPostId] = [];
            }
            bans[ban.banPostId].push(ban);
            return bans;
          }, {});
        });

      // im sure we'll need this again soon
      // let ratings = {};
      // if (userIsMod) {
      //   ratings = await Rating.getRatingsForPostsWithUsers(postIds);
      // } else {
      //   ratings = await Rating.getRatingsForPosts(postIds);
      // }
      const ratings = await Rating.getRatingsForPostsWithUsers(postIds);

      // dear god why
      const posts = queryResults.map(item => {
        const {
          postId,
          postContent,
          postUserId,
          postCreatedAt,
          postUpdatedAt,
          postCountryName,
          userUsername,
          userUsergroup,
          userAvatar_url,
          userBackgroundUrl,
          userCreated_at,
          userBanCount
        } = item;

        const MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN = 300000;
        const postIsOldEnoughForRatings =
          new Date().getTime() - new Date(postCreatedAt).getTime() >
          MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN;

        const ratingsContent = postIsOldEnoughForRatings ? ratings[postId] : [];

        const returnPost = {
          id: postId,
          content: convertSlateToBBCode(postContent),
          createdAt: postCreatedAt,
          updatedAt: postUpdatedAt,
          countryName: postCountryName,
          user: {
            id: postUserId,
            username: userUsername,
            usergroup: userUsergroup,
            avatar_url: userAvatar_url,
            backgroundUrl: userBackgroundUrl,
            createdAt: userCreated_at,
            isBanned: userBanCount > 0
          },
          bans: bans[postId],
          ratings: ratingsContent
        };

        return returnPost;
      });

      const {
        threadId,
        threadTitle,
        threadIconId,
        threadUserId,
        threadSubforumName,
        threadSubforumId,
        threadCreatedAt,
        threadUpdatedAt,
        threadDeletedAt,
        threadLocked,
        threadPinned,
        threadBackgroundUrl,
        threadBackgroundType
      } = queryResults[0];

      const user = await knex
        .from('Users')
        .select('username', 'usergroup')
        .where(knex.raw('id = ?', [threadUserId]));

      const thread = {
        id: threadId,
        title: threadTitle,
        iconId: threadIconId,
        userId: threadUserId,
        subforumId: threadSubforumId,
        subforumName: threadSubforumName,
        createdAt: threadCreatedAt,
        updatedAt: threadUpdatedAt,
        deleted: !!threadDeletedAt,
        locked: !!threadLocked,
        pinned: !!threadPinned,
        threadBackgroundUrl,
        threadBackgroundType,
        currentPage: pageParam || 1,
        user
      };

      const response: any = {
        ...thread,
        totalPosts: PAGE_POSTS,
        posts
      };

      if (req.isLoggedIn) {
        // get read threads
        const unreadPosts = await knex
          .select('thread_id', 'last_seen as lastSeen')
          .first()
          .from('ReadThreads')
          .whereRaw('ReadThreads.thread_id = ? AND ReadThreads.user_id = ?', [
            threadId,
            req.user.id
          ]);

        if (unreadPosts) {
          response.readThreadLastSeen = unreadPosts.lastSeen;
        }

        // get subscriptions (alerts)
        const subscriptions = await knex
          .select('thread_id', 'last_seen as lastSeen')
          .first()
          .from('Alerts')
          .whereRaw('Alerts.thread_id = ? AND Alerts.user_id = ?', [threadId, req.user.id]);

        response.isSubscribedTo = subscriptions && subscriptions.thread_id;
        if (response.isSubscribedTo) {
          response.subscriptionLastSeen = subscriptions.lastSeen;
        }
      }

      return ResponseStrategy.send(res, response);
    } else {
      console.log('Page has no posts. Return thread');

      // lets just repeat a ton of (BAD) code :^)
      const query = await knex
        .from('Threads as th')
        .select(
          'th.id as threadId',
          'th.title as threadTitle',
          'th.icon_id as threadIconId',
          'th.user_id as threadUserId',
          'th.subforum_id as threadSubforumId',
          'th.created_at as threadCreatedAt',
          'th.updated_at as threadUpdatedAt'
        )
        .where(knex.raw('th.id = ?', [req.params.id]));

      const {
        threadId,
        threadTitle,
        threadIconId,
        threadUserId,
        threadSubforumId,
        threadCreatedAt,
        threadUpdatedAt
      } = query;

      const thread = {
        id: threadId,
        title: threadTitle,
        iconId: threadIconId,
        userId: threadUserId,
        subforumId: threadSubforumId,
        createdAt: threadCreatedAt,
        updatedAt: threadUpdatedAt,
        currentPage: pageParam,
        totalPosts: PAGE_POSTS,
        posts: []
      };

      return ResponseStrategy.send(res, thread);
    }
  } catch (error) {
    console.error(error);
    ResponseStrategy.send(res, error);
  }
};

export const updateTags = async (req: Request, res: Response) => {
  if (req.user.isBanned) {
    throw new Error('User is banned');
  }

  if (!validateUserFields(req.user)) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({ error: 'Invalid user' });
    return;
  }

  // at this time only mods can edit an existing thread's tags
  const allowedUsergroups = [3, 4, 5];
  if (!allowedUsergroups.includes(req.user.usergroup)) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({ error: 'Invalid user' });
    return;
  }

  if (!req.body.threadId || !req.body.tags || req.body.tags.length > 3) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    return res.json({ error: 'Invalid content' });
  }
  console.log(req.body.threadId)

  // delete all existing tags for that thread
  await knex('ThreadTags')
    .delete()
    .where({
      thread_id: req.body.threadId
    });


  if (req.body.tags.length > 0) {
    const result = await knex('ThreadTags')
      .insert(req.body.tags
        .map(tag => (
          {
            tag_id: tag,
            thread_id: req.body.threadId
          }
        ))
      );
  }

  await invalidateObject(req.body.threadId);  

  res.status(httpStatus.OK);
  res.json({ message: 'Tags updated.' });
}
