import { Request, Response } from 'express';
import httpStatus from 'http-status';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import ratingList from '../helpers/ratingList.json';
import knex from '../services/knex';
import { POLITICS_SUBFORUM_ID } from '../../config/server';

const POLITICS_RATINGS = [];
const SHARED_POLITICS_RATINGS = [
  'agree',
  'disagree',
  'informative',
  'late',
  'sad',
  'idea',
  'winner',
  'glasses',
  'citation'
];

export const store = async (req: Request, res: Response) => {
  try {
    const ratingId = Object.keys(ratingList).find(ratingId => {
      return ratingList[ratingId].short == req.body.rating;
    });
    if (!ratingId) {
      console.log('invalid rating')
      throw new Error('Invalid rating.');
    }

    const post = await knex
      .select('Posts.user_id', 'thread_id', 'subforum_id')
      .from('Posts')
      .leftJoin('Threads', 'thread_id', 'Threads.id')
      .where('Posts.id', req.body.postId);

    if (!post[0]) {
      throw new Error('Invalid post.');
    }
    if (post[0].user_id === req.user.id) {
      throw new Error('Rating yourself is pretty sad.');
    }
    // using a Politics rating on a non-Politics subforum
    if (POLITICS_RATINGS.includes(req.body.rating) && post[0].subforum_id !== POLITICS_SUBFORUM_ID) {
      throw new Error('Attempted to use an invalid rating on the Politics subforum');
    }
    // using a non-Politics rating on a Politics subforum
    if (!POLITICS_RATINGS.includes(req.body.rating) && !SHARED_POLITICS_RATINGS.includes(req.body.rating) && post[0].subforum_id === POLITICS_SUBFORUM_ID) {
      throw new Error('Attempted to use an invalid rating outside of the Politics subforum');
    }

    const result = onDuplicateUpdate(knex, 'Ratings', {
      user_id: req.user.id,
      post_id: req.body.postId,
      rating_id: ratingId
    });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};
