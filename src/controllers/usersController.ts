import { Request, Response } from 'express';
import { default as ResponseStrategy } from "../helpers/responseStrategy";
import { getFormattedObjects } from '../retriever/user';
import httpStatus from 'http-status';
import knex from '../services/knex';

export const search = async (req: Request, res: Response) => {
  const offset = req.params.page ? Number(req.params.page) * 40 - 40 : 0;
  const filter = req.query.filter || null;

  const allUsers = await knex
    .from('Users as u')
    .count('u.id as count')
    .where((builder) => {
      if(!filter) return;
      builder.where('u.id', 'like', `%${filter}%`).orWhere('u.username', 'like', `%${filter}%`);
    })
    .first();

	const query = await knex
		.from('Users as u')
		.select('u.id')
    .where((builder) => {
      if(!filter) return;
      builder.where('u.id', 'like', `%${filter}%`).orWhere('u.username', 'like', `%${filter}%`);
    })
		.orderBy('u.id', 'desc')
		.limit(40)
		.offset(offset);

  const userIds = query.map((user) => {
    return user.id;
  });

  const users = await getFormattedObjects(userIds);
  return ResponseStrategy.send(res, {
    totalUsers: allUsers.count,
    currentPage: Number(req.params.page) || 1,
    users: users
  });
};