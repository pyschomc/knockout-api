import { Request, Response } from 'express';
import ResponseStrategy from '../helpers/responseStrategy';

export const ip = async (req: Request, res: Response) => {

  const data = {
    ip: "" + req.ipInfo,
    remoteAddress: req.connection.remoteAddress,
    headers: req.headers
  };

  return ResponseStrategy.send(res, data);

};