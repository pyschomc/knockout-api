import { createRedirectUrl } from '../helpers/utils';
import * as patreon from '@nathanhigh/patreon';

import { NODE_ENV, PATREON_CLIENT_ID, PATREON_CLIENT_SECRET, PATREON_CAMPAIGN_ID, PATREON_CREATOR_ACCESS_TOKEN } from '../../config/server';
import { Request, Response } from 'express';
import { URLSearchParams, URL } from 'url';
import knex from '../services/knex';
import { createGoldProductSubscriptionsForUser } from './productController';
import { object } from 'prop-types';

interface Tier {
  id: number,
  title: string
}

const tierNames = {
  1: `I'm really helping!`,
  2: `Leave it to me.`,
  3: `You're like a little baby.`,
  4: `Watch this.`
};

const getRedirectUrl = (req: Request) => {
  const protocol = (NODE_ENV === "development") ? 'http' : 'https';
  return createRedirectUrl(req, '/link/patreon', protocol);
};

function redirect(req: Request, res: Response) {
  const url = 'https://www.patreon.com/oauth2/authorize'
    + '?response_type=code'
    + '&scope=identity%20campaigns%20campaigns.members%20identity%5Bemail%5D'
    + '&client_id=' + encodeURIComponent(PATREON_CLIENT_ID)
    + '&redirect_url=' + encodeURIComponent(getRedirectUrl(req));
  res.redirect(302, url);
}

async function handleCallback(req: Request, res: Response) {
  try {
    const code: string = req.query.code;
    const oauth = patreon.oauth(PATREON_CLIENT_ID, PATREON_CLIENT_SECRET);
    const tokens = await oauth.getTokens(code, getRedirectUrl(req));
    const api = await patreon.patreon(tokens.access_token);
    const userInfo = await api('/identity?include=memberships');

    const patreonUserId: number = userInfo.rawJson.data.id;

    const memberships = userInfo.rawJson.data.relationships.memberships

    console.log(`Patreon membership for user ${req.user.username}: ${JSON.stringify(memberships.data)}`);

    // insert into DB. to be used later
    await linkPatreonAccountInDatabase(patreonUserId, req.user.id);

    // force into checkUserPledgeStatus with true
    if (memberships.data[0]) {
      const patreonMembershipId = memberships.data[0].id;

      const pledgeInfo = await api(`/members/${patreonMembershipId}?include=currently_entitled_tiers&fields%5Btier%5D=title`);

      const tiers = pledgeInfo.store.graph.tier;

      if (Object.values(tiers).length > 0) {
        const tierTitles = Object.values(tiers).map((el: Tier) => {
          return el.title;
        });

        const productIdArray = Array.from(
          Array(
            Object.values(tierNames)
              .findIndex(
                el => el === tierTitles[0]
              ) + 1
          ),
          (e, i) => i + 1
        );

        const callbackFn = (data: { error?: string, message?: string }) => {
          if (data.error) {
            res.status(500);
            return res.send(data.error);
          }

          res.status(200);
          res.send(data.message);
        }

        await createGoldProductSubscriptionsForUser(req.user, productIdArray, callbackFn);
      }
    } else {
      res.status(500);
      return res.send('No Patreon membership found.');
    }
  } catch (err) {
    console.error(`Error: ${JSON.stringify(err)}`);
    res.sendStatus(500);
  }
}

export async function link(req: Request, res: Response) {
  if (req.query.code) {
    return await handleCallback(req, res);
  }
  return redirect(req, res);
}

async function linkPatreonAccountInDatabase(patreonUserId: number, userId: number) {
  try {
    const existingRecords = await knex('ExternalAccounts')
      .select('user_id', 'id')
      .where({
        provider: 'PATREON',
        external_id: patreonUserId
      });

    // patreon accounts can only be used by one user
    existingRecords.forEach(row => {
      if (row.user_id !== userId) {
        throw new Error('Patreon account already used.');
      }
    });

    if (existingRecords.length === 0) {
      const result = await knex('ExternalAccounts').insert({
        provider: 'PATREON',
        user_id: userId,
        external_id: patreonUserId
      });
    }

    return true;
  } catch (error) {
    console.error(error)
    return false;
  }
}

export async function getUserIsLinkedToPatreon(req: Request, res: Response) {
  try {
    // throw new Error('Could not find linked Patreon account.');

    const result = await knex('ExternalAccounts')
      .select('id')
      .where('user_id', req.user.id)
      .where('provider', 'PATREON');

    console.log(result);

    res.status(200);
    res.json({
      userId: req.user.id,
      isLinkedToPatreon: result.length > 0
    });
  } catch (error) {
    res.status(500);
    res.json({ error: 'Could not get Patreon linking status.' });
  }
}