import { NextFunction, Request, Response } from "express";
import httpStatus from 'http-status';
import knex from '../services/knex';

const exactMatch = async (ip: string) => {
  const results = await knex
    .from('IpBans')
    .pluck('id')
    .where('address', ip);
  return (typeof results[0] != 'undefined');
};

const rangeMatch = async (ip: string) => {
  const Netmask = require("netmask").Netmask;
  const results = await knex
    .from('IpBans')
    .pluck('range')
    .whereNotNull('range');
  const ranges = results.reduce((list, range) => {
    const block = new Netmask(range);
    block.forEach((addr) => {
      list.push(addr);
    });
    return list;
  }, []).filter((elem, index, self) => {
    return index == self.indexOf(elem);
  });
  return ranges.indexOf(ip) > -1;
};

export default async (req: Request, res: Response, next: NextFunction) => {
  if (typeof req.ipInfo != 'undefined') {
    if (await exactMatch(req.ipInfo) || await rangeMatch(req.ipInfo)) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ message: 'You are banned, go away'});
      return;
    }
  }

  next();
}