import { Request, Response, NextFunction } from "express";

const CONTENT_FORMAT_VERSION = 1;

const contentFormatVersion = (req: Request, res: Response, next: NextFunction) => {
  try {
    const header = parseInt(req.headers['content-format-version'] as string, 10);

    if (!header || header !== CONTENT_FORMAT_VERSION) {
      throw new Error('Invalid content format version.');
    }

    return next();
  } catch (error) {
    res.status(422);
    res.send(
      'Invalid content format version. Please update your content format using the QA server.'
    );
  }
};

contentFormatVersion.CONTENT_FORMAT_VERSION = CONTENT_FORMAT_VERSION;

export default contentFormatVersion;
