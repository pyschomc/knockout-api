import { NextFunction, Request, Response } from "express";

export default (req: Request, res: Response, next: NextFunction) => {
  let xForwardedFor = (req.headers['x-forwarded-for'] || '');
  // Un-arrayify the parameter
  if (Array.isArray(xForwardedFor)) {
    xForwardedFor = xForwardedFor[0];
  }
  // Remove the port
  xForwardedFor = xForwardedFor.replace(/:\d+$/, '');
  // Grab the start of the chain
  const xForwardedParts = xForwardedFor.split(',')[0];
  xForwardedFor = (xForwardedParts.length > 0) ? xForwardedParts.trim() : xForwardedFor;
  // Get either the proxied address or the real remote address
  const ip = xForwardedFor || req.connection.remoteAddress;
  req.ipInfo = ip;
  next();
}