import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
const ratingList = require('../helpers/ratingList.json');

export default class Postrating extends AbstractRetriever {

  private cachePrefix: string = 'post-rating';

  private async getRatings(ids: Array<number>) {
    const results = await knex
      .from('Ratings as r')
      .select(
        'r.rating_id as ratingId',
        'r.post_id as postId',
        'u.username as username'
      )
      .leftJoin('Users as u', 'u.id', 'r.user_id')
      .whereIn('r.post_id', ids)
      .orderBy('r.rating_id', 'asc')
      .orderBy('r.updated_at', 'desc');

    return results.reduce((list, rating) => {
      if (typeof list[rating.postId] == 'undefined') list[rating.postId] = [];
      list[rating.postId].push(rating);
      return list;
    }, {});
  };

  private format(datum): Object {
    const byRating = datum.reduce((list, rating, index) => {
      if (typeof list[rating.ratingId] == 'undefined') list[rating.ratingId] = [];
      list[rating.ratingId].push(rating);
      return list;
    }, {});

    return Object.keys(byRating).map((ratingId) => {
      const data = byRating[ratingId];
      return {
        id: data[0].postId,
        rating: ratingList[ratingId].short,
        rating_id: Number(ratingId), // deprecate
        ratingId: Number(ratingId),
        users: data.map((rating) => { return rating.username; }),
        count: data.length
      };
    });
  };

  async get() {
    // grab canonical data
    const cachedRatings = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedRatings);
    const uncachedRatings = await this.getRatings(uncachedIds);
    const ratings = this.ids.map((id, index) => {
      if (cachedRatings[index] !== null) return cachedRatings[index];
      if (typeof uncachedRatings[id] != 'undefined') return this.format(uncachedRatings[id]);
      return null;
    }).filter((rating) => {
      return rating != null;
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      ratings.map(async (rating) => {
        await this.cacheSet(this.cachePrefix, rating[0].id, rating);
      });
    }

    return ratings;
  };

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  };

}