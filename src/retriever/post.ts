import AbstractRetriever from './abstractRetriever';
import User from './user';
import Thread from './thread';
import PostRating from './postRating';
import Ban from './ban';
import knex from '../services/knex';
import { postsPerPage } from '../constants/pagination';

export default class Post extends AbstractRetriever {

  private cachePrefix: string = 'post';

  public static INCLUDE_THREAD = 1;

  private async getPosts(ids: Array<number>) {
    const results = await knex
      .from('Posts as p')
      .select(
        'p.id as postId',
        'p.content as postContent',
        'p.created_at as postCreatedAt',
        'p.updated_at as postUpdatedAt',
        'p.user_id as postUserId',
        'p.thread_id as postThreadId'
      )
      .whereIn('p.id', ids)
      .groupBy('postId');

    return results.reduce((list, thread) => {
      list[thread.postId] = thread;
      return list;
    }, {});
  };

  private async getUsers(posts: Array<any>) {
    const userIds = posts.map((post) => {
      return post.user;
    });
    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private async getRatings(posts: Array<any>) {
    const postIds = posts.map((post) => {
      return post.id;
    });
    const ratingRetriever = new PostRating(postIds, []);
    const rawRatings = await ratingRetriever.get();
    return postIds.reduce((list, postId, index) => {
      list[postId] = rawRatings[index];
      return list;
    }, {});
  };

  private async getThreads(posts: Array<any>) {
    if (!this.hasFlag(Post.INCLUDE_THREAD)) return [];
    const threadIds = posts.map((post) => {
      return post.thread;
    }).filter((elem, index, self) => {
      return index == self.indexOf(elem);
    });
    const threadRetriever = new Thread(threadIds, []);
    const rawThreads = await threadRetriever.get();
    return threadIds.reduce((list, threadId, index) => {
      list[threadId] = rawThreads[index];
      return list;
    }, {});
  };

  private async getPageNumbers(posts: Array<any>) {
    const threadIds = posts.map((post) => {
      return post.thread;
    }).filter((elem, index, self) => {
      return index == self.indexOf(elem);
    });
    const postIds = posts.map((post) => {
      return post.id;
    }).filter((elem, index, self) => {
      return index == self.indexOf(elem);
    });
    const rawThreadPosts = await knex
      .from('Posts')
      .select(
        'id',
        'thread_id'
      )
      .orderBy('id', 'asc')
      .whereIn('thread_id', threadIds);
    const postIndex = rawThreadPosts.reduce((list, post) => {
      if (typeof list[post.thread_id] == 'undefined') list[post.thread_id] = [];
      list[post.thread_id].push(post.id);
      return list;
    }, {});
    const threadIndex = rawThreadPosts.reduce((list, post) => {
      list[post.id] = post.thread_id;
      return list;
    }, {});
    return postIds.reduce((list, postId) => {
      const postThreadId = threadIndex[postId];
      const threadPosts = postIndex[postThreadId];
      list[postId] = Math.ceil(threadPosts.indexOf(postId) / postsPerPage);
      return list;
    }, {});
  }

  private async getBans(posts: Array<any>) {
    const postIds = posts.map((post) => { return post.id; });
    const postBans = await knex
      .from('Bans')
      .select('id', 'post_id')
      .whereIn('post_id', postIds);
    const banIds = postBans.map((ban) => { return ban.id; });
    const banRetriever = new Ban(banIds, []);
    const bans = await banRetriever.get();
    const banIndex = bans.reduce((list, ban) => {
      list[ban.id] = ban;
      return list;
    }, {});
    return postBans.reduce((list, ban) => {
      if (typeof list[ban.post_id] == 'undefined') list[ban.post_id] = [];
      list[ban.post_id].push(banIndex[ban.id]);
      return list;
    }, {});
  }

  private format(data): Object {
    return {
      id: data.postId,
      thread: data.postThreadId,
      page: 1,
      content: data.postContent,
      created_at: data.postCreatedAt, // deprecate
      createdAt: data.postCreatedAt,
      updated_at: data.postUpdatedAt, // deprecate
      updatedAt: data.postUpdatedAt,
      user: data.postUserId,
      ratings: [],
      bans: []
    };
  };

  async get() {
    // grab canonical data
    const cachedPosts = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedPosts);
    const uncachedPosts = await this.getPosts(uncachedIds);

    const posts = this.ids.map((id, index) => {
      if (cachedPosts[index] !== null) return cachedPosts[index];
      return this.format(uncachedPosts[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      posts.map(async (post) => {
        await this.cacheSet(this.cachePrefix, post.id, post);
      });
    }

    // grab data from related caches
    const users = await this.getUsers(posts);
    const ratings = await this.getRatings(posts);
    const bans = await this.getBans(posts);
    const threads = await this.getThreads(posts);
    const pageNumbers = await this.getPageNumbers(posts);

    // merge related data in
    return posts.map((post) => {
      post.user = users[post.user] || post.user;
      post.ratings = ratings[post.id] || [];
      post.bans = bans[post.id] || [];
      post.thread = threads[post.thread] || post.thread;
      post.page = pageNumbers[post.id] || null;
      return post;
    });
  };

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  };

}

export const invalidateObjects = async (ids: Array<number>) => {
  const postRetriever = new Post(ids, []);
  await postRetriever.invalidate();
}

export const invalidateObject = async (id: number) => {
  const postRetriever = new Post([id], []);
  await postRetriever.invalidate();
}

export const getFormattedObjects = async (ids: Array<number>) => {
  const postRetriever = new Post(ids, []);
  return await postRetriever.get();
}

export const getFormattedObject = async (id: number) => {
  const postRetriever = new Post([id], []);
  const posts = await postRetriever.get();
  return posts[0];
}