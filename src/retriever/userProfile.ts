import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';

export default class Profile extends AbstractRetriever {

  protected static cacheLifetime = 86400;
  private cachePrefix: string = 'profile';

  private async getProfiles(ids: Array<number>) {
    const results = await knex
      .from('UserProfiles as p')
      .select(
        'p.user_id as userId',
        'p.heading_text as headingText',
        'p.personal_site as personalSite',
        'p.background_url as backgroundUrl',
        'p.background_type as backgroundType',
        'p.created_at as createdAt',
        'p.updated_at as updatedAt'
      )
      .whereIn('p.user_id', ids)
      .groupBy('userId');

    return results.reduce((list, profile) => {
      list[profile.userId] = profile;
      return list;
    }, {});
  }

  private format(data): Object {
    return {
      id: data.userId,
      userId: data.userId,
      headingText: data.headingText,
      personalSite: data.personalSite,
      backgroundUrl: data.backgroundUrl,
      backgroundType: data.backgroundType
    };
  }

  async get() {
    // grab canonical data
    const cachedProfiles = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedProfiles);
    const uncachedProfiles = await this.getProfiles(uncachedIds);
    const profiles = this.ids.map((id, index) => {
      if (cachedProfiles[index] !== null) return cachedProfiles[index];
      if (typeof uncachedProfiles[id] != 'undefined') return this.format(uncachedProfiles[id]);
      return {};
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      profiles.map(async (profile) => {
        await this.cacheSet(this.cachePrefix, profile.id, profile);
      });
    }

    return profiles;
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  };

}

// legacy signitures, remove at soonest convenience
export const invalidateObjects = async (ids: Array<number>) => {
  const profileRetriever = new Profile(ids, []);
  await profileRetriever.invalidate();
}

export const invalidateObject = async (id: number) => {
  const profileRetriever = new Profile([id], []);
  await profileRetriever.invalidate();
}

export const getFormattedObjects = async (ids: Array<number>) => {
  const profileRetriever = new Profile(ids, []);
  return await profileRetriever.get();
}

export const getFormattedObject = async (id: number) => {
  const profileRetriever = new Profile([id], []);
  const profiles = await profileRetriever.get();
  return profiles[0];
}