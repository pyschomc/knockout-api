import AbstractRetriever from './abstractRetriever';
import User from './user';
import Post from './post';
import Thread from './thread';
import knex from '../services/knex';

import redis from '../services/redisClient';
import { getFormattedObjects as getFormattedUserObjects } from '../retriever/user';
import { getFormattedObjects as getFormattedPostObjects } from '../retriever/post';

export default class Report extends AbstractRetriever {

  private cachePrefix: string = 'report';

  private async getReports(ids: Array<number>) {
    const results = await knex('Reports')
      .select(
        'id',
        'post_id as postId',
        'reported_by as reportedBy',
        'report_reason as reportReason',
        'solved_by as solvedBy',
        'created_at as createdAt'
      )
    .whereIn('id', ids);

    return results.reduce((list, report) => {
      list[report.id] = report;
      return list;
    }, {});
  }

  private async getUsers(reports: Array<any>) {
    const userIds = reports.reduce((list, report) => {
      list.push(report.reportedBy);
      list.push(report.solvedBy);
      return list;
    }, []).filter((elem, index, self) => {
      return index == self.indexOf(elem);
    });

    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private async getPosts(reports: Array<any>) {
    const postIds = reports.map((report) => {
      return report.post;
    }).filter((elem, index, self) => {
      return index == self.indexOf(elem);
    });

    const postRetriever = new Post(postIds, []);
    const rawPosts = await postRetriever.get();
    return rawPosts.reduce((list, post) => {
      list[post.id] = post;
      return list;
    }, {});
  }

  private format(data): Object {
    return {
      id: data.id,
      createdAt: data.createdAt,
      reason: data.reason,
      post: data.postId,
      reportedBy: data.reportedBy,
      solvedBy: data.solvedBy
    };
  };

  async get() {
    // grab canonical data
    const cachedReports = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedReports);
    const uncachedReports = await this.getReports(uncachedIds);
    const reports = this.ids.map((id, index) => {
      if (cachedReports[index] !== null) return cachedReports[index];
      return this.format(uncachedReports[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      reports.map(async (report) => {
        await this.cacheSet(this.cachePrefix, report.id, report);
      });
    }

    // grab data from related caches
    const users = await this.getUsers(reports);
    const posts = await this.getPosts(reports);

    // merge related data in
    return reports.map((report) => {
      report.reportedBy = users[report.reportedBy] || null;
      report.solvedBy = users[report.solvedBy] || null;
      report.post = posts[report.post] || null;
      return report;
    });
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }

}

export const invalidateObjects = async (ids: Array<number>) => {
  const ReportRetriever = new Report(ids, []);
  await ReportRetriever.invalidate();
}

export const invalidateObject = async (id: number) => {
  const ReportRetriever = new Report([id], []);
  await ReportRetriever.invalidate();
}

export const getFormattedObjects = async (ids: Array<number>) => {
  const ReportRetriever = new Report(ids, []);
  return await ReportRetriever.get();
}

export const getFormattedObject = async (id: number) => {
  const ReportRetriever = new Report([id], []);
  const reports = await ReportRetriever.get();
  return reports[0];
}