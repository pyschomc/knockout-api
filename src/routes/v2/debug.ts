import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

router.get('/debug', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;