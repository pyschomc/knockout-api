import express from 'express';
import multer from "multer";

const storage = multer.memoryStorage()
const upload = multer({ storage });

import errorHandler from '../services/errorHandler';

import {
  redisMiddleware,
  authMiddleware,
  contentFormatVersion
} from '../middleware';

import {
  subforumGetAllController,
  subforumGetThreadsController,
  reportGetController,
  reportCreateController,
  reportResolveController,
  reportsGetAllController,
  reportsOpenCountController,
  postController,
  threadController,
  authController,
  usersController,
  userController,
  subforumController,
  ratingsController,
  banController,
  alertController,
  moderationController,
  imageController,
  statsController,
  eventLogController,
  searchController,
  mentionsController,
  readThreadController,
  debugController,
  patreonController,
  userProfileController,
  tagController,
  threadAdController
} from '../controllers';

const catchErrors = errorHandler.catchErrors;
const authentication = authMiddleware.authentication;
const router = express.Router();

router.use(redisMiddleware.verifyCache);

// Auth
  router.get('/auth/google/login', authController.google.login);
  router.get('/auth/google/callback', authController.google.callback);
  router.get('/auth/twitter/login', authController.passport.twitter);
  router.get('/auth/twitter/callback', authController.passport.twitter);
  router.get('/auth/github/login', authController.passport.github);
  router.get('/auth/github/callback', authController.passport.github);
  router.get('/auth/steam/login', authController.passport.steam);
  router.get('/auth/steam/callback', authController.passport.steam);
  router.post('/auth/logout', authController.logout);
  router.get('/auth/finish', authController.finish);

// Patreon linking
  router.get('/link/patreon', authentication, patreonController.link);

// Stats
  router.get('/stats', statsController.index);

// Users
  router.get('/users/:page?', authentication.optional, catchErrors(usersController.search));

// User
router.get('/user', authentication.optional, userController.index);
router.put('/user', contentFormatVersion, authentication, userController.update);
// router.get('/user/authCheck', authentication, userController.authCheck); // deprecated
router.get('/user/syncData', authentication.optional, userController.syncData);
router.put('/user/profile', authentication.optional, upload.single('backgroundImage'), userProfileController.update);
router.put('/user/updateProfileRatingsDisplay', authentication, userController.updateProfileRatingsDisplay);
router.get('/user/profileRatingsDisplay', authentication, userController.getProfileRatingsDisplay);
router.get('/user/bans/:id', authentication.optional, userController.getUserBans); // deprecate?
router.get('/user/:id', authentication.optional, catchErrors(userController.show));
router.get('/user/:id/bans', authentication.optional, userController.getBans);
router.get('/user/:id/threads/:page?', authentication.optional, userController.getThreads);
router.get('/user/:id/posts/:page?', authentication.optional, userController.getPosts);
router.get('/user/:id/topRatings', authentication.optional, userController.getTopRatings);
router.get('/user/:id/profile', authentication.optional, userProfileController.show);
router.get('/user/profile/:id', authentication.optional, userProfileController.show); // deprecate
router.delete('/user/:id', authentication, userController.deleteOwnAccount);

// Posts
  router.get('/post/:id', authentication.optional, postController.withPostsAndCount);
  router.post('/post', contentFormatVersion, authentication, catchErrors(postController.store));
  router.put('/post', contentFormatVersion, authentication, catchErrors(postController.update));

// Threads
  router.get('/thread/latest', catchErrors(threadController.latest));
  router.get('/thread/popular', catchErrors(threadController.popular));
  router.get('/popular-threads', catchErrors(threadController.popular));
  router.get('/thread/:id/:page?', authentication.optional, threadController.withPostsAndCount);
  router.post('/thread', contentFormatVersion, authentication, catchErrors(threadController.store));
  router.put('/thread', contentFormatVersion, authentication, catchErrors(threadController.update));
  router.put('/thread/tags', contentFormatVersion, authentication, catchErrors(threadController.updateTags));

// Subforums
  router.get('/subforum', catchErrors(subforumGetAllController.getAll));
  router.get('/subforum/:id/:page?', authentication.optional, catchErrors(subforumGetThreadsController.getThreads));

// Ratings
  router.put('/rating', contentFormatVersion, authentication, catchErrors(ratingsController.store))

// Bans
  router.post('/ban', authentication, catchErrors(banController.store))

// Alerts
  router.post('/alert', authentication, catchErrors(alertController.store));
  router.delete('/alert', authentication, catchErrors(alertController.destroy));
  router.post('/alert/list', authentication, catchErrors(alertController.index))

// Read Threads
  router.post('/readThreads', contentFormatVersion, authentication, catchErrors(readThreadController.store));
  router.delete('/readThreads', contentFormatVersion, authentication, catchErrors(readThreadController.destroy));
  router.post('/readThreads/list', contentFormatVersion, authentication, catchErrors(readThreadController.index))

// Reports
  router.get('/report/:id', authentication, catchErrors(reportGetController.get));
  router.post('/report/:id/resolve', authentication, catchErrors(reportResolveController.post));
  router.post('/report/create', authentication, catchErrors(reportCreateController.post));
  router.post('/report', contentFormatVersion, authentication, catchErrors(reportCreateController.post)); // deprecate
  router.get('/reports/:page?', authentication, catchErrors(reportsGetAllController.get));
  router.get('/reports/open-count', authentication, catchErrors(reportsOpenCountController.get));

// Images
  router.post('/avatar', contentFormatVersion, authentication, upload.single('image'), catchErrors(imageController.avatarUpload));
  router.post('/background', contentFormatVersion, authentication, upload.single('image'), catchErrors(imageController.backgroundUpload));
  router.get('/image/:filename', catchErrors(imageController.show));

// Events
  router.get('/events', catchErrors(eventLogController.index));

// Mentions
  router.post('/mentions/get', contentFormatVersion, authentication, catchErrors(mentionsController.index));
  router.put('/mentions', contentFormatVersion, authentication, catchErrors(mentionsController.markAsRead));

// Tags
  router.post('/tag', contentFormatVersion, authentication, catchErrors(tagController.store));
  router.get('/tag/list', catchErrors(tagController.index));

// Moderation
  // RIP REST
  router.post('/moderation/ipsByUser', authentication, catchErrors(moderationController.getIpsByUsername));
  router.post('/moderation/usersByIp', authentication, catchErrors(moderationController.getUsernamesByIp));
  router.post('/moderation/openReports', authentication, catchErrors(moderationController.getOpenReports)); // deprecate
  router.post('/moderation/closeReport', authentication, catchErrors(moderationController.closeReport)); // deprecate
  router.post('/moderation/changeThreadStatus', authentication, catchErrors(moderationController.changeThreadStatus));
  router.post('/moderation/countOpenReports', authentication, catchErrors(moderationController.countOpenReports)); // deprecate
  router.post('/moderation/removeUserImage', authentication, catchErrors(moderationController.removeUserImage));
  router.post('/moderation/removeUserProfile', authentication, catchErrors(moderationController.removeUserProfile));
  router.get('/moderation/getLatestUsers', authentication, catchErrors(moderationController.getLatestUsers));
  router.get('/moderation/getDashboardData', authentication, catchErrors(moderationController.getDashboardData));
  router.post('/moderation/getFullUserInfo', authentication, catchErrors(moderationController.getFullUserInfo));
  router.post('/moderation/makeBanInvalid', authentication, catchErrors(moderationController.makeBanInvalid));
  router.post('/moderation/addGoldProduct', authentication, catchErrors(moderationController.addGoldProduct));

// Search
  router.post('/search', catchErrors(searchController.query));
  router.get('/search/results/:nonce', catchErrors(searchController.results));

// Thread Ads
  router.get('/threadAds/list', authentication.optional, catchErrors(threadAdController.index));
  router.get('/threadAds/random', authentication.optional, catchErrors(threadAdController.random));

// Debug
  router.get('/debug/ip', catchErrors(debugController.ip));

// Misc
  router.get('/static/scripts/*', express.static('.'));
  router.get('/static/reference.html', express.static('.'));
  router.get('/version', (req, res) => res.json({
    contentFormatVersion: contentFormatVersion.CONTENT_FORMAT_VERSION,
    // TODO: API version information (ie. git commit id and date)
  }));

export default router;
