import { Data } from 'slate';

const validateFirstLevelKeys = (slateObject: Data) => {
  try {
    const keys = Object.keys(slateObject);
    if (keys.length === 2 && keys[0] === 'object' && keys[1] === 'document') {
      return true;
    }
  } catch (error) {
    return false;
  }
};

const validateFirstLevelNodes = (slateObject: Data) => {
  try {
    const documentNodes = slateObject.document.nodes;

    for (let i = 0; i < documentNodes.length; i++) {
      if (!validSlateTypes.includes(documentNodes[i].type)) {
        throw new Error('Invalid node detected. Aborting.');
      }
    }

    return true;
  } catch (error) {
    return false;
  }
};

const validateDeepNodes = (slateObject: Data) => {
  try {
    let types = [];
    const documentNodes = slateObject.document.nodes;

    const recurse = array => {
      for (let i = 0; i < array.length; i++) {
        const el = array[i];

        if (el.type && !validSlateTypes.includes(el.type)) {
          throw new Error('Invalid Slate document. Aborting. 44');
        }
        if (el.type && !types.includes(el.type)) {
          types.push(el.type);
        }
        if (el.nodes && !Array.isArray(el.nodes)) {
          throw new Error('Invalid Slate document. Aborting. 51');
        }

        if (el.nodes) {
          recurse(el.nodes);
        }
        if (el.leaves) {
          recurse(el.leaves);
        }
        if (el.marks) {
          recurse(el.marks);
        }
      }
    };

    recurse(documentNodes);
    return true;
  } catch (error) {
    console.log('Error in slate validation:64 -> ', error);
    return false;
  }
};

export { validateFirstLevelKeys, validateFirstLevelNodes, validateDeepNodes };

const validSlateTypes = [
  'block-quote',
  'bulleted-list',
  'heading-one',
  'heading-two',
  'list-item',
  'numbered-list',
  'image',
  'youtube',
  'strawpoll',
  'twitter',
  'link',
  'video',
  'userquote',
  'bold',
  'code',
  'italic',
  'underlined',
  'spoiler',
  'paragraph'
];
