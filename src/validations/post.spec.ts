import { validatePostLength, validateThreadStatus } from './post';
import { insertThreads, deleteThreads } from '../../test/helper/dbPosts';
import {
  validSlatePost,
  validSlateLongPost,
  invalidSlateLongPost,
  validSlatePostWithAllTypes,
  invalidSlatePostWithWrongFirstLevelProperty,
  invalidSlatePostWithAllTypesButOneIsNotAllowed,
  invalidSlatePostContentTooShort,
  validSlatePostContentJustAnImage,
  validSlatePostContentJustAnImageNoParagraphs
} from '../../test/helper/slateContent';

describe('Test slate post content validation', () => {
  test('validation passes for a regular post', () => {
    expect(validatePostLength(validSlatePost)).toBe(true);
  });

  test('validation passes for a longer post under 8000 characters', () => {
    expect(validatePostLength(validSlateLongPost)).toBe(true);
  });

  test('validation fails for a very long post over 8000 characters', () => {
    expect(validatePostLength(invalidSlateLongPost)).toBe(false);
  });

  test('validation passes for a post with most (if not all) node types', () => {
    expect(validatePostLength(validSlatePostWithAllTypes)).toBe(true);
  });

  test('validation fails for a post with most (if not all) node types but one is invalid', () => {
    expect(validatePostLength(invalidSlatePostWithAllTypesButOneIsNotAllowed)).toBe(false);
  });

  test('validation fails for a post with an invalid first level property', () => {
    expect(validatePostLength(invalidSlatePostWithWrongFirstLevelProperty)).toBe(false);
  });

  test('validation fails for a post with short content (nothing at all)', () => {
    expect(validatePostLength(invalidSlatePostContentTooShort)).toBe(false);
  });

  test('validation passes for a post with just an image inserted (no user input other than pasting image url)', () => {
    expect(validatePostLength(validSlatePostContentJustAnImage)).toBe(true);
  });

  test('validation passes for a post with just an image inserted (automatic paragraphs removed)', () => {
    // this is indeed the expected behavior. to improve on this, we should be converting non-text
    // nodes to something like [image: https://image.com/image.jpg] or [youtube: https://youtube.com/v/youtube]
    // and have this validated on the frontend as well.
    expect(validatePostLength(validSlatePostContentJustAnImageNoParagraphs)).toBe(false);
  });
});

describe('Test thread status validation', () => {
  afterEach(async () => {
    await deleteThreads();
  });

  test('validation passes for a valid thread', async () => {
    await insertThreads({
      title: 'test',
      created_at: new Date(),
      updated_at: new Date()
    });

    expect(await validateThreadStatus(1)).toBe(true);
  });

  test('validation fails for a locked thread', async () => {
    await insertThreads({
      title: 'test',
      created_at: new Date(),
      updated_at: new Date(),
      locked: true
    });

    expect(await validateThreadStatus(1)).toBe(false);
  });

  test('validation fails for a deleted thread', async () => {
    await insertThreads({
      title: 'test',
      created_at: new Date(),
      updated_at: new Date(),
      deleted_at: new Date()
    });

    expect(await validateThreadStatus(1)).toBe(false);
  });

  test('validation fails for a nonexisting thread', async () => {
    expect(await validateThreadStatus(9999)).toBe(false);
  });
});
