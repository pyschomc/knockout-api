import { Response } from 'express';
import httpStatus from 'http-status';

interface Result {
  [key: string]: any;
  message?: string;
}

interface Options {
  page: number;
}

class ResponseStrategy {

  result: Result;

  constructor(result: Result) {
    this.result = result;
  }

  sendAction(res: Response, options: Options) {
    throw Error('Response strategy not defined.');
  }
}

class ErrorResponse extends ResponseStrategy {

  result: Result;

  sendAction(res: Response) {
    switch (this.result.message) {
      case 'Validation Error':
        res.status(httpStatus.UNPROCESSABLE_ENTITY);
        res.json({ message: 'Unprocessable entity.' });
        break;
      default:
        res.status(httpStatus.SERVICE_UNAVAILABLE);
        res.json({ message: 'Service unavailable.' });
    }
  }
}

class NotFoundResponse extends ResponseStrategy {

  result: Result;

  sendAction(res: Response, options: Options) {
    res.status(httpStatus.NOT_FOUND);
    return res.json({
      message: 'Resource not found.',
    });
  }
}

class ResponseOK extends ResponseStrategy {

  result: Result;

  sendAction(res: Response, options: Options) {
    res.status(httpStatus.OK);
    return res.json(this.result);
  }
}

class PaginatedResponse extends ResponseStrategy {

  result: Result;

  sendAction(res: Response, options: Options) {
    if (options.page === 1) {
      res.status(httpStatus.OK);
      return res.json(this.result);
    }
    return new NotFoundResponse(this.result).sendAction(res, options);
  }
}

const createResponseStrategy = () => {
  const selectStrategy = (result: Result) => {
    switch (true) {
      case result === null: {
        return new NotFoundResponse(result);
      }

      case typeof result === 'object' && 'list' in result: {
        return new ResponseOK(result);
      }

      case result instanceof Error: {
        return new ErrorResponse(result);
      }

      case 'list' in result: {
        return new PaginatedResponse(result);
      }

      default: {
        return new ResponseOK(result);
      }
    }
  };

  return {
    send(response: Response, result: Result, options?: Options) {
      selectStrategy(result).sendAction(response, options);
    },
  };
};

export default createResponseStrategy();
