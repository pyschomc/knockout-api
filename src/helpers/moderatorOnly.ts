import { Request, Response } from "express";
import httpStatus from "http-status";

const allowedUsergroups = [3, 4, 5];

const moderatorOnly = (req: Request, res: Response) => {
  if (!req.isLoggedIn || !req.user || !allowedUsergroups.includes(req.user.usergroup) || req.user.isBanned) {
    res.status(httpStatus.FORBIDDEN);
    res.json({ error: "Forbidden" });
    throw new Error("Not authorized.");
  }
};

export default moderatorOnly;
