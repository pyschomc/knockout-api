'use strict';

import { lorem, date, address, internet } from 'faker';

const THREAD_POST_COUNT_MAX = 400;

const randomInt = (max) => {
  return Math.floor(Math.random() * (max - 1) + 1);
}

const randomPost = (userId, threadId) => {
  return {
    content: lorem.paragraph(),
    user_id: userId,
    thread_id: threadId,
    created_at: date.past(1),
    updated_at: date.past(1),
    ip_address: internet.ip(),
    country_name: address.country(),
    app_name: 'knockout.chat',
  }
}

const insertPosts = async (queryInterface, threadIds, userIds) => {
  // create posts in random threads by random users
  let i, j, temparr, chunkSize = 250;
  for (i = 0, j = threadIds.length; i < j; i+=chunkSize) {
    temparr = threadIds.slice(i, i+chunkSize);

    const posts = temparr.map((threadId) => {
      const postCount = randomInt(THREAD_POST_COUNT_MAX);

      return Array.from({ length: postCount }).map(() => {
        const userId = userIds[randomInt(userIds.length)];
        return randomPost(userId, threadId);
      })
    })

    await queryInterface.bulkInsert('Posts', [].concat(...posts));
  }
  return true;
}

export async function up(queryInterface, Sequelize) {
  const threadIds = await queryInterface.sequelize.query(
    `SELECT id FROM Threads`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  const userIds = await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  return insertPosts(queryInterface, threadIds, userIds)
}
export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Posts', null, {});
}
