// FIXME: this shouldn't be in the global scope, especially
// since ES6 imports happen before the current script is run,
// so anything else that's imported from this script that
// runs code in the global scope will see global.__basedir as
// undefined.
global.__basedir = process.cwd();

import cluster from 'cluster';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import fs from 'fs';
import helmet from 'helmet';
import compression from 'compression';
import session from 'express-session';
import redis from './src/services/redisClient';
import http from 'http';
import https from 'https';
import limits from 'limits';

import rateLimiterMiddleware from './src/middleware/rateLimit';
import ipInfo from './src/middleware/ip';
import ipBan from './src/middleware/ipBan';
import datasource from './src/models/datasource';
import legacyRoute from './src/routes';
import errorHandler from './src/services/errorHandler';
import os from 'os';

import authRoute from './src/routes/v2/auth';
import subforumRoute from './src/routes/v2/subforum';
import threadRoute from './src/routes/v2/thread';
import postRoute from './src/routes/v2/post';
import userRoute from './src/routes/v2/user';
import statRoute from './src/routes/v2/stat';
import searchRoute from './src/routes/v2/search';
import debugRoute from './src/routes/v2/debug';
import validOrigins from './config/validOrigins';

import { JWT_SECRET } from './config/server';

const coreCount = os.cpus().length;
const RedisStore = require('connect-redis')(session);

console.log(process.env.MODERATION_WEBHOOK);
console.log('awoo')


let clusterWorkers = parseInt(process.env.CLUSTER_WORKERS, 10);
if (!isFinite(clusterWorkers)) {
  // default value
  clusterWorkers = coreCount;
}

const limits_config = {
  enable: true,
  file_uploads: true,
  post_max_size: 5000000
};

const app = express();
app.datasource = datasource();
app.use(helmet());
app.use(compression());
app.use(session({
  store: new RedisStore({ client: redis }),
  secret: JWT_SECRET
}));

if (process.env.NODE_ENV !== 'production') {
  validOrigins.push('http://localhost:8080');
  validOrigins.push('http://localhost:8081');
}

app.use(
  cors({
    origin: validOrigins,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    exposedHeaders: 'Content-Range,X-Content-Range,X-Total-Count',
    credentials: true
  })
);

app.use(rateLimiterMiddleware);
app.use(ipInfo);
app.use(ipBan);
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cookieParser());
app.use(limits(limits_config));

app.use('/', legacyRoute);
app.use('/v2/', authRoute);
app.use('/v2/', debugRoute);
app.use('/v2/', postRoute);
app.use('/v2/', searchRoute);
app.use('/v2/', statRoute);
app.use('/v2/', subforumRoute);
app.use('/v2/', threadRoute);
app.use('/v2/', userRoute);

errorHandler.catchHandler(app);

if (clusterWorkers > 0 && cluster.isMaster) {
  console.log(`Starting ${clusterWorkers} Workers...`);
  // log events
  cluster.on('online', (worker) => {
    console.log('Worker ' + worker.process.pid + ' is online.');
  });
  cluster.on('exit', (worker, code, signal) => {
    console.log('Worker ' + worker.process.pid + ' died.');
    cluster.fork();
  });
  // fork processes
  for (let i = 0; i < clusterWorkers; i++) {
    cluster.fork();
  }
} else {

  if (process.env.NODE_ENV === 'production') {
    const httpServer = http.createServer(app);
    httpServer.listen(8002, () => {
      console.log('🚀 HTTP Server running on port 8002');
    });
  }
  if (process.env.NODE_ENV === 'qa') {
    const privateKey = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/privkey.pem`,
      'utf8'
    );
    const certificate = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/cert.pem`,
      'utf8'
    );
    const ca = fs.readFileSync(
      `/etc/letsencrypt/live/${process.env.CERT_DOMAIN}/fullchain.pem`,
      'utf8'
    );
    const credentials = {
      key: privateKey,
      cert: certificate,
      ca: ca
    };
    const httpsServer = https.createServer(credentials, app);
    httpsServer.listen(3000, () => {
      console.log('🚀 HTTPS Server running on port 3000');
    });
  }
  if (process.env.NODE_ENV === 'development') {
    const httpServer = http.createServer(app);
    httpServer.listen(3000, () => {
      console.log('🚀 HTTP Server running on port 3000');
    });
  }
}

module.exports = app;
